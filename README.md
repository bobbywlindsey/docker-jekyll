# docker-jekyll

Set up an automated Jekyll blog environment using Docker

Copy the latest gemfile changes from blog:

`cp ~/Dropbox/me/career/website-and-blog/bobbywlindsey/Gemfile* .`

Any changes to the Dockerfile will be automatically built in Docker Hub, so just pull the container:

`docker pull bobbywlindsey/docker-jekyll`

Then run the container:

`docker-compose run --service-ports site`
